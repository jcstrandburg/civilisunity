﻿using UnityEngine;
using System.Collections;

public class ResourceReservation : Reservation {
    public GameObject source;
    public string resourceTag;
    public double amount;
}
