using System;

/// <summary>
/// Indicates that a class should be serialized through the CustomObjectSerializationSurrogate
/// </summary>
/// <author>Justin Strandburg</author>
public class CustomSerialize : Attribute {	
}
